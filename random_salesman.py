from random import shuffle, seed
from base_salesman import BaseSalesman
import time
import sys


MAX_TIME = 25


class Salesman(BaseSalesman):
    def run(self, file_path=None, max_time=MAX_TIME):
        start_time = time.time()
        seed(0)

        # read input
        self._read_input(file_path)

        # randomly generate path
        cities = list(range(1, len(self.city_2_ind)))
        bestPath = []
        bestPrice = sys.maxsize
        while time.time() - start_time < max_time:
            shuffle(cities)
            path = [self.home] + cities + [self.home]

            # check if path exists and get price
            price = 0
            for d in range(len(path) - 1):
                fr = path[d]
                to = path[d + 1]

                p = self.timetable[d][fr][to]
                if p == -1:
                    break
                else:
                    price += p

            if price < bestPrice:
                bestPrice = price
                bestPath = path
        if bestPrice == sys.maxsize:
            bestPrice = None
        return bestPath, bestPrice


def main(argv):
    rs = Salesman()
    if len(argv) == 1:
        best_path, best_price = rs.run(file_path=argv[0], max_time=MAX_TIME)
    else:
        best_path, best_price = rs.run(max_time=MAX_TIME)

    rs._print_output(best_path, best_price)


if __name__ == '__main__':
    main(sys.argv[1:])
