from random import seed, randint, shuffle, random  # noqa
from deap import base, creator, tools
from base_salesman import BaseSalesman

import numpy as np
import sys
import time


MAX_TIME = 28
PB_MUTATION = 0.2
PB_CROSSOVER = 0.5
PB_KBMI = 0.2
N_POPULATION = 32
N_TOURNAMENT_WINNERS = 16
BROKEN_PENALTY = 10000

VERBOSE = False


class Salesman(BaseSalesman):

    def run(self,
            file_path=None,
            max_time=MAX_TIME,
            pb_mutation=PB_MUTATION,
            pb_crossover=PB_CROSSOVER,
            pb_kbmi=PB_KBMI,
            n_population=N_POPULATION,
            n_tournament_winners=N_TOURNAMENT_WINNERS):
        start_time = time.time()
        # seed(0)
        # numpy.random.seed(0)

        self._read_input(file_path)
        self.timetable[self.timetable == 0] = BROKEN_PENALTY

        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMin)

        toolbox = base.Toolbox()

        toolbox.register("population", self._gen_population, len(self.city_2_ind))

        toolbox.register("evaluate", self._fitness)
        toolbox.register("mate", self._crossover)
        toolbox.register("op_kbmi", self._mutate_knowledge_based_multiple_inversion)
        toolbox.register("op_kbns", self._mutate_knowledge_based_neighbor_swap)
        toolbox.register("mutate", self._mutate_simple_inversion)
        toolbox.register("select", tools.selTournament, tournsize=n_tournament_winners)

        population = toolbox.population(size=n_population)
        hall_of_fame = tools.HallOfFame(1)
        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)

        gen_time = max_time - (time.time() - start_time)
        if gen_time < 0:
            return None, None

        self._eaSimple(population, toolbox, pb_crossover, pb_mutation, pb_kbmi,
                       gen_time, stats=stats, halloffame=hall_of_fame, verbose=VERBOSE)

        # prepare result
        bestPrice = self._fitness(hall_of_fame[0])[0]
        bestPath = [self.home] + hall_of_fame[0] + [self.home]

        if VERBOSE:
            print('Time: {}'.format(time.time() - start_time))

        return bestPath, bestPrice

    def _gen_population(self, cities, size):
        pop = []

        path = [self.home]
        visited = set()
        visited.add(self.home)
        for d in range(cities - 1):
            minVal = BROKEN_PENALTY
            minC = 0
            for c in range(cities):
                if c not in visited:
                    val = self.timetable[path[d], c, d]
                    if val < minVal:
                        minC = c
                        minVal = val
            path.append(minC)
            visited.add(minC)
        path.remove(self.home)

        for i in range(size):
            if i % 3 == 0:
                pop.append(creator.Individual(path.copy()))
            elif i % 3 == 1:
                pop.append(creator.Individual(list(reversed(path)).copy()))
            else:
                rand_path = path.copy()
                shuffle(rand_path)
                pop.append(creator.Individual(rand_path))
        return pop

    def _fitness(self, individual):
        path = [self.home] + individual + [self.home]
        price = sum([self.timetable[path[d], path[d + 1], d] for d in range(len(path) - 1)])
        return (price,)

    def _crossover(self, individual1, individual2):
        a = randint(0, len(individual1))
        b = randint(0, len(individual1))
        while a == b or abs(a-b):
            b = randint(0, len(individual1))
        a, b = min(a, b), max(a, b)

        set1_allowed = set(individual1[a:b])
        set2_allowed = set(individual2[a:b])

        individual1[a:b] = [x for x in individual2 if x in set1_allowed]
        individual2[a:b] = [x for x in individual1 if x in set2_allowed]

        return individual1, individual2

    def _mutate_knowledge_based_multiple_inversion(self, individual):
        path = [self.home] + individual + [self.home]

        # create list of flights sorted by price
        flights = []
        for i in range(len(path) - 1):
            flights.append((i, self.timetable[path[i], path[i + 1], i]))
        flights = sorted(flights, key=lambda x: x[1], reverse=True)

        # we dont need price, take only most expeisive flights
        flights = [x[0] for x in flights][:2 + len(flights) // 10]

        # inverting phase
        for s in range(2):
            p = randint(0, len(flights) - 1)
            q = randint(0, len(flights) - 1)

            a = min(flights[p], flights[q])  # start of inverting sequence
            b = max(flights[p], flights[q])  # end of inverting sequence (b is excluded)

            if abs(a - b) <= 1:
                continue

            individual[a:b] = list(reversed(individual[a:b]))
            break

        return individual,

    def _mutate_knowledge_based_neighbor_swap(self, individual):
        path = [self.home] + individual + [self.home]
        a = randint(1, len(individual))
        #timetable_a = self.timetable[a]
        #timetable_a_prev = self.timetable[a-1][path[a-1]]
        path_a = path[a]
        path_a_prev = path[a-1]
        timetable_path_a = self.timetable[path_a]
        timetable_path_a_prev = self.timetable[path_a_prev]

        best_price = sys.maxsize
        best_b = None
        for b in range(1, len(individual) + 1):
            if a - 1 <= b <= a + 1 or b - 1 <= a <= b + 1:
                continue

            price = timetable_path_a_prev[path[b], a - 1] \
                    + self.timetable[path[b], path[a + 1], a] \
                    + self.timetable[path[b - 1], path_a, b - 1] \
                    + timetable_path_a[path[b + 1], b]

            if price < best_price:
                best_price = price
                best_b = b

        if best_b:
            a -= 1
            best_b -= 1

            individual[a], individual[best_b] = individual[best_b], individual[a]

        return individual,

    def _mutate_simple_inversion(self, individual):
        a = randint(0, len(individual) - 1)
        b = randint(0, len(individual) - 1)
        while a == b:
            b = randint(0, len(individual) - 1)

        individual[a:b] = list(reversed(individual[a:b]))

        return individual,

    @staticmethod
    def _eaSimple(population, toolbox, cxpb, mutpb, kbmipb, max_time, stats=None,
                  halloffame=None, verbose=__debug__):
        """This algorithm reproduce the simplest evolutionary algorithm as
        presented in chapter 7 of [Back2000]_.

        :param population: A list of individuals.
        :param toolbox: A :class:`~deap.base.Toolbox` that contains the evolution
                        operators.
        :param cxpb: The probability of mating two individuals.
        :param mutpb: The probability of mutating an individual.
        :param max_time: Maximum time spent by algorithm - can be overstepped by
                one generation
        :param stats: A :class:`~deap.tools.Statistics` object that is updated
                      inplace, optional.
        :param halloffame: A :class:`~deap.tools.HallOfFame` object that will
                           contain the best individuals, optional.
        :param verbose: Whether or not to log the statistics.
        :returns: The final population and a :class:`~deap.tools.Logbook`
                  with the statistics of the evolution.
        """
        start_time = time.time()

        logbook = tools.Logbook()
        logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in population if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        if halloffame is not None:
            halloffame.update(population)

        record = stats.compile(population) if stats else {}
        logbook.record(gen=0, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)

        # Begin the generational process
        gen = 0
        while time.time() - start_time < max_time:
            phase = (time.time() - start_time) / max_time


            for ind in population:
                # KBMI
                if random() < 0.001 + (0.999 - phase) * kbmipb:
                    ind, = toolbox.op_kbmi(ind)
                    del ind.fitness.values

                # fitness
                if not ind.fitness.valid:
                    ind.fitness.values = toolbox.evaluate(ind)

            # Select the next generation individuals
            offspring = toolbox.select(population, len(population))

            # Vary the pool of individuals
            offspring = [toolbox.clone(ind) for ind in offspring]

            # crossover
            for i in range(len(offspring)):
                if random() < cxpb and False:
                    j = i
                    while i == j:
                        j = randint(0, len(offspring) - 1)
                    offspring[i], offspring[j] = toolbox.mate(offspring[i], offspring[j])
                    del offspring[i].fitness.values, offspring[j].fitness.values


            for ind in offspring:
                # KBNS
                ind, = toolbox.op_kbns(ind)
                del ind.fitness.values

                # Mutation - inversion
                if random() < 0.001 + (0.999 - phase) * mutpb:
                    ind, = toolbox.mutate(ind)
                    del ind.fitness.values

                # Evaluate the individuals with an invalid fitness
                if not ind.fitness.valid:
                    ind.fitness.values = toolbox.evaluate(ind)

            # Update the hall of fame with the generated individuals
            if halloffame is not None:
                halloffame.update(offspring)

            # Replace the current population by the offspring
            population[:] = offspring

            # Append the current generation statistics to the logbook
            record = stats.compile(population) if stats else {}
            logbook.record(gen=gen, nevals=len(invalid_ind), **record)
            if verbose:
                print(logbook.stream)
            gen += 1

        return population, logbook


def main(argv):
    gs = Salesman()
    if len(argv) == 1:
        best_path, best_price = gs.run(file_path=argv[0], max_time=MAX_TIME)
    else:
        best_path, best_price = gs.run(max_time=MAX_TIME)

    gs._print_output(best_path, best_price)


if __name__ == '__main__':
    main(sys.argv[1:])
