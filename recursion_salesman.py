#python recursion_salesman.py ./dataset/5.txt
import sys
import copy
import time

input_cities = set()
reduced_flights = dict()
input_start_city = ''
best_route = []
best_price = float('inf')

def find_ways(city, cities_to_go, day, route, price):	
	#print(city + " " + str(cities_to_go) + " " + str(day) + " " + str(route) + " " + str(price))
	global best_route
	global best_price
	if (price > best_price):
		return(0)
	if len(cities_to_go) == 0:
		for f in reduced_flights:
			if (f[0] == city) and (f[2] == day) and (f[1] in input_start_city):
				new_route = copy.copy(route)
				new_route.append(f[1])
				final_price = price + reduced_flights[f]
				#print("výsledná cesta: " + str(new_route) + " za " + str(final_price))
				if (final_price < best_price):
					best_price = final_price
					best_route = new_route
				return(final_price)
	else:		
		for f in reduced_flights:
			if (f[0] == city) and (f[2] == day) and (f[1] in cities_to_go):				
				new_cities = copy.copy(cities_to_go)
				new_cities.remove(f[1])
				new_route = copy.copy(route)
				new_route.append(f[1])
				find_ways(f[1], new_cities, day + 1, new_route, price + reduced_flights[f])				

try:
    input = open(sys.argv[1], 'r')
except:
    print("Could not open input file")
    sys.exit(1)

input_flights = dict()
input_start_city = input.readline().rstrip()

for l in input.readlines():
    [f, t, d, p] = l.rstrip().split(' ') #From_city, To_city, Day, Price
    d = int(d)
    p = int(p)

    if (f, t, d) in input_flights:
        print("Input contains two same flights at same day")
        sys.exit(1)
    else:
    	input_flights[(f, t, d)] = p
    input_cities.add(f)
    input_cities.add(t)

#print(input_cities)
print(len(input_flights))

from_cities = set()
to_cities = set()
from_cities.add(input_start_city)
for i in range (0, len(input_cities)):
	for f in input_flights:
		if f[2] == i:
			if f[0] in from_cities:
				reduced_flights[f] = input_flights[f]
				to_cities.add (f[1])
	from_cities = to_cities
	to_cities = set()

print(len(reduced_flights))
#print(reduced_flights)

cities = input_cities
cities.remove(input_start_city)

start_time = time.time()

find_ways(input_start_city, cities, 0, [input_start_city], 0)

print("Nejlepší cesta: " + str(best_route) + " za " + str(best_price))

end_time = time.time()
print("It has been {0} seconds since the loop started".format(end_time - start_time))