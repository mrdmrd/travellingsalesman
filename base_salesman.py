from sys import stdin
import numpy as np
import pandas as pan
import time


class BaseSalesman():
    timetable = {}
    home = None     # indice of home
    ind_2_city = {}
    city_2_ind = {}

    def run(self, file_path=None, max_time=30):
        '''
        :param file_path: None for stdin, or path to file with flights
        Returns best solution and price. E.g. (['RUN', 'FOR', 'FUN'], 666)
        TODO maybe - would be good to optionally set loaded data to process
        '''
        raise RuntimeError('Salesman\'s method run hasn\'t been implemented.')

    def _read_input(self, file_path=None):
        '''
        :param file_path: None for stdin, or path to file with flights
        needs data sorted by specification - does not verify
        '''
        start_time = time.time()  # noqa
        if file_path:
            try:
                input = open(file_path, 'r')
            except Exception as e:
                print('Cannot read file {}. Exception {}.'.format(file_path, str(e)))
        else:
            input = stdin

        self.home = input.readline().rstrip()

        # prepare structures
        frame = pan.read_csv(input,
                             sep=' ',
                             names=['fr', 'to', 'day', 'price'],
                             index_col=[0, 1, 2],
                             dtype={
                                 'fr': 'category',
                                 'to': 'category',
                                 'day': np.uint16,
                                 'price': np.uint32}
                             )   # day can be category too -1s to 300

        self.cities = frame.index.get_level_values('fr').unique().tolist()

        shape = list(map(len, frame.index.levels))
        self.timetable = np.full(shape, 0, np.uint32)
        self.timetable[frame.index.labels] = frame.values.flat

        # index dictionaries
        for n, city in enumerate(frame.index.levels[0]):
            self.city_2_ind[city] = n
            self.ind_2_city[n] = city

        #translate home
        self.home = self.city_2_ind[self.home]

        # print('structures prepared: {}'.format(time.time() - start_time))

    def _print_output(self, best_path, best_price):
        '''
        :param best_path: list of indices to airports
        :param best_price: price of best path
        '''
        if best_path:
            print(best_price)
            for d in range(len(best_path) - 1):
                fr = best_path[d]
                to = best_path[d + 1]
                print('{} {} {} {}'.format(self.ind_2_city[fr], self.ind_2_city[to], d, self.timetable[fr][to][d]))
        else:
            print('Cannot find path.')
