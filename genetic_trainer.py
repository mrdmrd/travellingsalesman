from multiprocessing.pool import Pool
import genetic_salesman
import sys
import random


# settigns
FILE = '/dev/shm/dataset/real/data_300.txt'
STEP = 0.1
TESTS = 5

# global variables
run = True


def test(params):
    salesman = genetic_salesman.Salesman()

    try:
        sum = 0
        for i in range(TESTS):
            _, err_result = salesman.run(FILE,
                                         pb_mutation=params[0],
                                         pb_crossover=params[1],
                                         pb_kbmi=params[2],
                                         n_population=int(params[3]),
                                         n_tournament_winners=int(params[4]))
            sum += err_result
        err_result = sum / TESTS
    except:
        err_result = None

    return err_result


def signal_handler(signal, frame):
    global run
    run = False


def main(argv):
    random.seed(0)
    global run

    pool = Pool(processes=1)

    # pb_mutation pb_crossover pb_kbmi n_population winners
    best_settings = [0.5, 0.5, 0.5, 20, 20]
    best_err = sys.maxsize
    params = len(best_settings)
    norm_c = [1/1, 1/1, 1/1, 1/40, 1/40]
    actual_settings = best_settings

    while run:
        norm_set = [actual_settings[i] * norm_c[i] for i in range(params)]

        norm_inc_set = norm_set.copy()
        norm_dec_set = norm_set.copy()

        for i in range(params):
            val = random.uniform(-1, 1) * STEP
            norm_inc_set[i] += val
            norm_dec_set[i] -= val

        inc_set = [norm_inc_set[i] / norm_c[i] for i in range(params)]
        dec_set = [norm_dec_set[i] / norm_c[i] for i in range(params)]

        async_result = pool.apply_async(test, args=[inc_set])
        dec_err = test(dec_set)
        inc_err = async_result.get()

        print('inc_err: {} dec_err {}'.format(inc_err, dec_err))

        if not inc_err and not dec_err:
            print('Dead point')
            continue
        elif not inc_err:
            inc_err = dec_err + 1
        elif not dec_err:
            dec_err = inc_err + 1

        if inc_err < dec_err:
            actual_err = inc_err
            actual_settings = inc_set

        else:
            actual_err = dec_err
            actual_settings = dec_set

        if actual_err < best_err:
            best_err = actual_err
            best_settings = actual_settings

        print('actual: {} err: {}'.format(actual_settings, min(inc_err, dec_err)))
        print('best: {} err: {}'.format(best_settings, best_err))


if __name__ == '__main__':
    main(sys.argv[1:])
