import random
import string
import os

OUT_DIR='dataset'
N_CITIES = [9]

def random_cities(n_cities):
    cities = set()
    while len(cities) < n_cities:
        city = "".join(random.choice(string.ascii_uppercase) for __ in range(3))
        cities.add(city)
    return list(cities)

def main():
    if not os.path.exists(OUT_DIR):
        os.makedirs(OUT_DIR)

    for n in N_CITIES:
        print('Generating {}'.format(n))
        cities = random_cities(n)

        with open(os.path.join(OUT_DIR,'{}.txt'.format(n)), 'w') as f:
            f.write('{}\n'.format(random.choice(cities)))

            for src in cities:
                for dst in cities:
                    if src == dst:
                        continue
                    for day in range(n):
                        f.write('{} {} {} {}\n'.format(src, dst, day, random.randint(10, 1500)))

if __name__ == '__main__':
    main()