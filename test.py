from math import log2
from subprocess import check_output, STDOUT, TimeoutExpired
import os
import sys


SALESMAN = 'genetic_salesman.py'
TEST_DIR = 'dataset/real'


def filename_to_n_cities(test_file):
    filename = os.path.split(test_file)[1]
    name = os.path.splitext(filename)[0]
    if name.find('data_') == 0:
        name = name[len('data_'):]
    return int(name)


def test(salesman, test_file):
    '''
    :param salesman: path to salesman py file
    :param test_file: path to test file
    :return: (score, price)
    '''
    # get weight of test
    cities = filename_to_n_cities(test_file)
    weight = log2(cities)

    # run test
    try:
        output = check_output(
            ('python3 {} {}'.format(SALESMAN, test_file)).split(),
            stderr=STDOUT,
            timeout=30)
    except TimeoutExpired:
        return None

    try:
        price = float(output.decode().split()[0])
    except:
        print('Unexpected output of salesman.')
        return None

    if not price:
        return None

    # find min and max of price
    min_price = sys.maxsize
    max_price = 0
    with open(test_file, 'r') as f:
        for line in f:
            words = line.split()
            if len(words) == 4:
                min_price = min(min_price, float(words[3]))
                max_price = max(max_price, float(words[3]))
    average = price / cities

    return (weight * (1 - average / (max_price - min_price)), price)


def main():
    # find all test files
    test_files = []
    for path, subdirs, files in os.walk(TEST_DIR):
        for name in files:
            test_files.append(os.path.join(path, name))

    # sort test files
    test_files = sorted(test_files, key=filename_to_n_cities)

    # do tests
    score = 0
    print('File Price Score')
    for path in test_files:
        result = test(SALESMAN, path)
        if result:
            test_score, test_price = result
            score += test_score
            print('{} {:0.0f} {:0.2f}'.format(path, test_price, test_score))
        else:
            print('{} Failed'.format(path))

    print('Final score {:0.2f}'.format(score))

if __name__ == '__main__':
    main()
